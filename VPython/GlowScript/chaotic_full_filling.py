from vpython import *
#GlowScript 2.7 VPython

scene.caption = """Right button drag or Ctrl-drag to rotate "camera" to view scene.
To zoom, drag with middle button or Alt/Option depressed, or use scroll wheel.
     On a two-button mouse, middle is left + right.
Shift-drag to pan left/right and up/down.
Touch screen: pinch/extend to zoom, swipe or two-finger rotate."""

side = 4.0
thk = 0.3
s2 = 2*side - thk
s3 = 2*side + thk

wallR = box (pos=vector( side, 0, 0), size=vector(thk, 1.2*s2, s3),  color = color.red,  axis=vector(1, 0.2, 0 )  )
wallL = box (pos=vector(-side, 0, 0), size=vector(thk, s2, s3),  color = color.red)
wallB = box (pos=vector(0, -side, 0), size=vector(1.2*s3, thk, s3),  color = color.blue)
wallT = box (pos=vector(0,  side, 0), size=vector(s3, thk, s3),  color = color.blue)
wallBK = box(pos=vector(0, 0, -side), size=vector(1.2*s2, s2, thk), color = color.gray(0.7))

ball = sphere (color = color.green, radius = 0.4, make_trail=True, retain=10000)
ball.mass = 1.0
ball.p = vector (-0.15, 0.1, +0)

side = side - thk*0.5 - ball.radius


def reflectionVector(d,n):
    n = n/mag(n)
    r = d - 2 * dot(d, n) * n
    return r;


dt = 0.3
while True:
    rate(200)
    ball.pos = ball.pos + (ball.p/ball.mass)*dt
    #if not (side > ball.pos.x > -side):
    #    ball.p.x = -ball.p.x
    sideR = side - 1/5 * ball.pos.y
    if not (sideR > ball.pos.x > -side):
        ball.p = reflectionVector(ball.p,wallR.axis)
    if not (side > ball.pos.y > -side):
        ball.p.y = -ball.p.y
    if not (side > ball.pos.z > -side):
        ball.p.z = -ball.p.z