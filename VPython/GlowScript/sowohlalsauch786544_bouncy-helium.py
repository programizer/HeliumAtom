from vpython import *
#GlowScript 2.7 VPython
# from math import *
# from visual import *
# from visual.graph import * 
from vpython import *
# import vpython

scene.fullscreen = True


# length units: 10^-12 m
# time units:   10^-12 s (~ 1s in Simulation, unless adjusted for further slow-motion...)
t = 0
dt = 0.00001

# Coulomb constant:
G = 8.99 * 10 ** 9

# Radii not to scale, just for good visualization
# No spin so far

spheres = [
sphere(pos=vector(0,0,0),radius =6,color=color.red,charge=2,mass=7200,velocity=vector(0,0,0),a = vector(0,0,0),trail=curve(color=color.red), name='Proton'),
sphere(pos=vector(52.9,0,0),radius=2.5,color=color.blue,charge=-1,mass=1,velocity=vector(0,0,0),a=vector(0,0,0),trail=curve(color=color.blue), name='Electron1'),
#sphere(pos=vector(0,12,0),radius=.08,color=color.green,mass=sqrt(4),velocity=vector(1.2,0,0.6),a=vector(0,0,0),trail=curve(color=color.green)),
sphere(pos=vector(-52.9,0,0),radius=2.5,color=color.white,charge=-1,mass=1,velocity=vector(0,0,0),a=vector(0,0,0),trail=curve(color=color.white), name='Electron2'),
#sphere(pos=vector(0,28,0),radius=.4,color=color.orange,mass=sqrt(80),velocity=vector(0.7,0,0.4),a=vector(0,0,0),trail=curve(color=color.orange)),
#sphere(pos=vector(0,32,0),radius=0.2,color=color.white,mass=-sqrt(10),velocity=vector(1.5,0,0.4),a=vector(0,0,0),trail=curve(color=color.white))
]

# Stop Electron2 until first bounce
firstBounce=False



#print(len(spheres))


def acceleration1on2(sphere2,sphere1):
    r = sphere2.pos - sphere1.pos
    r_mag = mag(r)
    normal_r = norm(r)
    g = ((G*sphere1.charge*sphere2.charge)/pow(r_mag,2))/sphere2.mass*normal_r
    return g
    
def simpleBounce(sphere2,sphere1):
    r = sphere2.pos - sphere1.pos
    r_mag = mag(r)
    if r_mag<8.5:
        sphere2.velocity = -sphere2.velocity - sphere2.a*dt #remove acceleration
        global firstBounce
        firstBounce=True
    return
    
    

while 1:
    rate(1000)
    
    # Halt Electron2 till first bounce
    if (not firstBounce):
        spheres[2].charge=0
    else:
        spheres[2].charge=-1
        
        
    for i in spheres:
        i.a = vector(0,0,0)
        for j in spheres:
            if i!=j:
                i.a = i.a + acceleration1on2(i,j)
                simpleBounce(i,j)
            
                          

        
    for i in spheres:        
        i.velocity = i.velocity + i.a *dt
        i.pos = i.pos+i.velocity*dt        
        i.trail.append(pos=i.pos)

            
    scene.center=vector(spheres[0].pos.x,spheres[0].pos.y,spheres[0].pos.z)

